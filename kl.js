let AWS = require('aws-sdk');
const cognito_idp = new AWS.CognitoIdentityServiceProvider();
const translate = new AWS.Translate();
const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {
    try {
        let data = await translate.translateText({
            SourceLanguageCode: "es",
            TargetLanguageCode: "en",
            Text: "Hola"
        }).promise();
        console.log(data);
    } catch (err) {
        // error handling goes here
    };
    
    return { "message": "Successfully executed" };
};